# Developer Stuff for Product Owners/Managers

A loose set of notes for Product Owners and Product Managers on how developers think, what we work on, and a taste of the culture too. The format is a work-in-progress, but some of the questions we hope to answer include things like:

* Ember versus Angular versus React?
* Tabs versus spaces? Emacs versus vi?
* What are unit tests? How do they compare to functional tests? Why do I want them?

And many more.

## Outline of this project

`notes/` will include rough transcripts and notes from discussions had and questioned asked.
`articles/` will include files. Each file contains focused discussions on a topic or question.